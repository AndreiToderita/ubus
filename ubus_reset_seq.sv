`ifndef __UBUS_RESET_SEQ_SV__
`define __UBUS_RESET_SEQ_SV__

typedef class ubus_base_sequence;

class ubus_base_seq_rst extends ubus_base_sequence;
	`uvm_object_utils(ubus_base_seq_rst)
	
	uvm_event reset_ev;
	e_reset_state reset_state;
	
	function new(string name = "ubus_base_seq_rst");
		super.new(name);
	endfunction
	
	virtual task body();
		reset_ev = uvm_event_pool::get_global("reset_ev");
		run_sequence();
	endtask
	
	virtual function void config_sequence();
	endfunction

	virtual task run_sequence();
		`uvm_warning(get_type_name(),"Empty Base Reset Sequence");
	endtask
	
endclass

class c_ubus_reset_seq extends uvm_sequence#(c_ubus_reset_pkt);
	`uvm_object_utils(c_ubus_reset_seq)
	
	int nr_reset_during_sim;
	
	function new(string name = "ubus_reset_seq");
		super.new(name);
		set_automatic_phase_objection(1);
	endfunction
	
	virtual task body();
		void'(uvm_config_db#(int)::get(null,get_full_name(),"nr_reset_during_sim", nr_reset_during_sim));
		
		//vom da un reset initial
		`uvm_do_with(req,{req.reset_state == RESET_ASSERTED;
						  req.transmit_time == 0;
						  req.reset_time == 5;})
		
		for(int count_reset = 0; count_reset < nr_reset_during_sim; count_reset++)begin
			`uvm_do_with(req,{req.reset_state == RESET_ASSERTED;
						  	 req.transmit_time inside {[50:150]};
						  	 req.reset_time == 3;})
		end
	endtask
	
endclass
`endif
