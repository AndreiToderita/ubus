`ifndef __VIRTUAL_SEQ_SV__
`define __VIRTUAL_SEQ_SV__

class c_virtual_seq extends uvm_sequence;
    `uvm_object_utils(c_virtual_seq)
    `uvm_declare_p_sequencer(c_virtual_sequencer)

    uvm_event reset_ev;
    e_reset_state reset_state;
    uvm_sequence#(ubus_transfer) slave_sequences[$];
    uvm_sequence#(ubus_transfer) master_sequences[$];

    function new(string name = "virtual_sequence");
        super.new(name);
    endfunction

    virtual task body();
        this.reset_ev = uvm_event_pool::get_global("reset_ev");
        reset_state = RESET_DEASSERTED;
        
        do begin
            reset_state = RESET_DEASSERTED;

            for(int i = 0; i < slave_sequences.size(); i++)begin
                fork
                    automatic int count = i;
                    slave_sequences[count].start(p_sequencer.slaves_sequencer[count]);
                join_none
            end

            if( master_sequences.size() > 0)
                fork
                    begin
                        for(int i = 0; i < master_sequences.size(); i++)begin
                            fork
                                automatic int count = i;
                                master_sequences[count].start(p_sequencer.masters_sequencer[count]);
                            join_none
                        end
                        wait fork;
                    end
                    begin
                        this.reset_ev.wait_trigger();
                        reset_state = RESET_ASSERTED;
                    end
                join_any
            for(int i = 0; i < p_sequencer.slaves_sequencer.size(); i++)begin
                p_sequencer.slaves_sequencer[i].stop_sequences();
            end
            for(int i = 0; i < p_sequencer.masters_sequencer.size(); i++)begin
                p_sequencer.masters_sequencer[i].stop_sequences();
            end
            disable fork;
        end while(reset_state != RESET_DEASSERTED);
    endtask
endclass

`endif
