
`ifndef __UBUS_RESET_AGENT_SV__
`define __UBUS_RESET_AGENT_SV__

typedef uvm_sequencer#(c_ubus_reset_pkt) c_ubus_reset_sequencer;

class c_ubus_reset_agent extends uvm_agent;
	`uvm_component_utils(c_ubus_reset_agent)
	
	protected e_reset_val reset_val;
	
	c_ubus_reset_driver ubus_reset_driver;
	c_ubus_reset_sequencer ubus_reset_sequencer;
	c_ubus_reset_monitor ubus_reset_monitor;
	
	function new(string name = "", uvm_component parent = null);
		super.new(name, parent);
	endfunction
	
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		ubus_reset_monitor = c_ubus_reset_monitor::type_id::create("ubus_reset_monitor", this);
		
		if(this.is_active == UVM_ACTIVE)begin
			ubus_reset_driver = c_ubus_reset_driver::type_id::create("ubus_reset_driver", this);
			ubus_reset_sequencer = c_ubus_reset_sequencer::type_id::create("ubus_reset_sequencer", this);
		end
		
		if(!uvm_config_db#(e_reset_val)::get(null, get_full_name(),"reset_val",reset_val))begin
			`uvm_fatal(get_type_name(),"Reset Val invalid");
		end
		uvm_config_db#(e_reset_val)::set(this,"*","reset_val",this.reset_val);
	endfunction
	
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		if(this.is_active == UVM_ACTIVE)begin
			ubus_reset_driver.seq_item_port.connect(ubus_reset_sequencer.seq_item_export);
		end
	endfunction
endclass
`endif