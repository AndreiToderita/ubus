

module dut_dummy(
		input wire[`N_MASTERS-1:0] ubus_req_master,
		output reg[`N_MASTERS-1:0] ubus_gnt_master,
		input wire ubus_clock,
		input wire ubus_reset,
		input wire [15:0] ubus_addr,
		input wire [1:0] ubus_size,
		output reg ubus_read,
		output reg ubus_write,
		output reg  ubus_start,
		input wire ubus_bip,
		inout wire [7:0] ubus_data,
		input wire ubus_wait,
		input wire ubus_error);

	reg[2:0]   st;
	reg[`N_MASTERS-1:0] gnt_count_acces; 
	reg gnt_check;

	// Basic arbiter, supports two masters, 0 has priority over 1
	function integer check_ubus_req(integer a);
		for(int cnt = 0; cnt < `N_MASTERS; cnt++)begin
			if(ubus_req_master[cnt] === 1'b1)begin
				return 1;
			end
		end
		return 0;
	endfunction

	always @(posedge ubus_clock or posedge ubus_reset) begin
		if(ubus_reset) begin
			ubus_start <= 1'b0;
			st<=3'h0;
		end
		else
			case(st)
				0: begin //Begin out of Reset
					ubus_start <= 1'b1;
					st<=3'h3;
				end
				3: begin //Start state
					ubus_start <= 1'b0;
					if(! ( check_ubus_req(0) )) begin
						st<=3'h4;
					end
					else begin
						st<=3'h1;
					end
				end
				4: begin // No-op state
					ubus_start <= 1'b1;
					st<=3'h3;
				end
				1: begin // Addr state
					st<=3'h2;
					ubus_start <= 1'b0;
				end
				2: begin // Data state
					if((ubus_error==1) || ((ubus_bip==0) && (ubus_wait==0))) begin
						st<=3'h3;
						ubus_start <= 1'b1;
					end
					else begin
						st<=3'h2;
						ubus_start <= 1'b0;
					end
				end
			endcase
	end 

	always@(ubus_reset, ubus_clock, ubus_start, ubus_req_master,gnt_count_acces)begin
		if(ubus_reset)begin
			ubus_gnt_master = {`N_MASTERS{1'b0}};
			gnt_count_acces = 0;
		end else begin
			if( (!ubus_clock) && ubus_start && check_ubus_req(0))begin
				gnt_check = 0;
				while(gnt_check != 1)begin
					if(ubus_req_master[gnt_count_acces] === 1'b1)begin
						gnt_check = 1;
						ubus_gnt_master[gnt_count_acces] = 1'b1;
					end else begin
						gnt_count_acces = ((gnt_count_acces + 1) == `N_MASTERS) ?  0 : gnt_count_acces + 1 ;
					end
				end
				gnt_count_acces = ((gnt_count_acces + 1) == `N_MASTERS) ?  0 : gnt_count_acces + 1 ;
			end else if( (!ubus_clock) && (!ubus_start) ) begin
				ubus_gnt_master = {`N_MASTERS{1'b0}};
			end
		end
	end

	always @(posedge ubus_clock or posedge ubus_reset) begin
		if(ubus_reset) begin
			ubus_read <= 1'bZ;
			ubus_write <= 1'bZ;
		end
		else if(ubus_start && !(|ubus_gnt_master)) begin
			ubus_read <= 1'b0;
			ubus_write <= 1'b0;
		end
		else begin
			ubus_read <= 1'bZ;
			ubus_write <= 1'bZ;
		end
	end

endmodule

