`ifndef __UBUS_RESET_PKT_SVH__
`define __UBUS_RESET_PKT_SVH__

typedef enum{
	RESET_ASSERTED = 0,
	RESET_DEASSERTED = 1
}e_reset_state;

typedef enum{
	ACTIVE_LOW = 0,
	ACTIVE_HIGH = 1
}e_reset_val;

class c_ubus_reset_pkt extends uvm_sequence_item;
	
	rand int transmit_time;
	rand int reset_time;
	
	e_reset_state reset_state;
	
	`uvm_object_utils_begin(c_ubus_reset_pkt)
	`uvm_field_int(transmit_time, UVM_DEFAULT)
	`uvm_field_int(reset_time, UVM_DEFAULT)
	`uvm_field_enum(e_reset_state, reset_state, UVM_DEFAULT)
	`uvm_object_utils_end
	
	function new(string name = "ubus_reset_pkt");
		super.new(name);
	endfunction
	
	
endclass

`endif
