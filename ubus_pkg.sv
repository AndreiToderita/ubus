package ubus_pkg;

   import uvm_pkg::*;

`include "uvm_macros.svh"

   typedef uvm_config_db#(virtual ubus_if) ubus_vif_config;
   typedef virtual ubus_if ubus_vif;

`include "ubus_transfer.sv"
`include "ubus_reset_pkt.sv"

`include "virtual_sequencer.sv"
`include "virtual_seq.sv" 

`include "ubus_reset_seq.sv"
`include "ubus_reset_driver.sv"
`include "ubus_reset_monitor.sv"
`include "ubus_reset_agent.sv"
`include "ubus_reset_seq.sv"

`include "ubus_master_seq_lib.sv"
`include "ubus_example_master_seq_lib.sv"
`include "ubus_master_monitor.sv"
`include "ubus_master_sequencer.sv"
`include "ubus_master_driver.sv"
`include "ubus_master_agent.sv"

`include "ubus_slave_sequencer.sv"
`include "ubus_slave_seq_lib.sv"
`include "ubus_slave_monitor.sv"
`include "ubus_slave_driver.sv"
`include "ubus_slave_agent.sv"

`include "ubus_bus_monitor.sv"

`include "ubus_env.sv"

`include "ubus_example_tb.sv"
`include "test_lib.sv"

endpackage: ubus_pkg

