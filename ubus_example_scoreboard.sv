
class ubus_example_scoreboard extends uvm_scoreboard;
	
	uvm_analysis_imp#(ubus_transfer, ubus_example_scoreboard) item_collected_export;

	protected bit disable_scoreboard = 0;
	protected int num_writes = 0;
	protected int num_init_reads = 0;
	protected int num_uninit_reads = 0;
	int sbd_error = 0;

	protected int unsigned m_mem_expected[int unsigned];

	// Provide implementations of virtual methods such as get_type_name and create
	`uvm_component_utils_begin(ubus_example_scoreboard)
		`uvm_field_int(disable_scoreboard, UVM_DEFAULT)
		`uvm_field_int(num_writes, UVM_DEFAULT|UVM_DEC)
		`uvm_field_int(num_init_reads, UVM_DEFAULT|UVM_DEC)
		`uvm_field_int(num_uninit_reads, UVM_DEFAULT|UVM_DEC)
	`uvm_component_utils_end

	// new - constructor
	function new (string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	//build_phase
	function void build_phase(uvm_phase phase);
		item_collected_export = new("item_collected_export", this);
	endfunction

	// write
	virtual function void write(ubus_transfer trans);
		if(!disable_scoreboard)
		begin
			memory_verify(trans);
		end
	endfunction : write


	// memory_verify
	protected function void memory_verify(input ubus_transfer trans);
		int unsigned data, exp;
		for (int i = 0; i < trans.size; i++) begin
			// Check to see if entry in associative array for this address when read
			// If so, check that transfer data matches associative array data.
			if (m_mem_expected.exists(trans.addr + i)) begin
				if (trans.read_write == READ) begin
					data = trans.data[i];
					`uvm_info(get_type_name(),
						$sformatf("%s to existing address...Checking address : %0h with data : %0h",
							trans.read_write.name(), trans.addr, data), UVM_LOW)
					assert(m_mem_expected[trans.addr + i] == trans.data[i]) else begin
						exp = m_mem_expected[trans.addr + i];
						`uvm_error(get_type_name(),
							$sformatf("Read data mismatch.  Expected : %0h.  Actual : %0h",
								exp, data))
						sbd_error = 1;
					end
					num_init_reads++;
				end
				if (trans.read_write == WRITE) begin
					data = trans.data[i];
					`uvm_info(get_type_name(),
						$sformatf("%s to existing address...Updating address : %0h with data : %0h",
							trans.read_write.name(), trans.addr + i, data), UVM_LOW)
					m_mem_expected[trans.addr + i] = trans.data[i];
					num_writes++;
				end
			end
			// Check to see if entry in associative array for this address
			// If not, update the location regardless if read or write.
			else begin
				data = trans.data[i];
				`uvm_info(get_type_name(),
					$sformatf("%s to empty address...Updating address : %0h with data : %0h",
						trans.read_write.name(), trans.addr + i, data), UVM_LOW)
				m_mem_expected[trans.addr + i] = trans.data[i];
				if(trans.read_write == READ)
					num_uninit_reads++;
				else if (trans.read_write == WRITE)
					num_writes++;
			end
		end
	endfunction : memory_verify

	// report_phase
	virtual function void report_phase(uvm_phase phase);
		if(!disable_scoreboard) begin
			`uvm_info(get_type_name(),
				$sformatf("Reporting scoreboard information...\n%s", this.sprint()), UVM_LOW)
		end
	endfunction : report_phase

endclass : ubus_example_scoreboard

class mutex_scoreboard extends ubus_example_scoreboard;
	`uvm_component_utils(mutex_scoreboard)

	logic[7:0] mutex_memory;
	int mutex_addr;
	int start_addr;
	int stop_addr;

	function new (string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		void'(uvm_config_db#(int)::get(null,get_full_name(),"mutex_addr",this.mutex_addr));
		void'(uvm_config_db#(int)::get(null,get_full_name(),"start_addr",this.start_addr));
		void'(uvm_config_db#(int)::get(null,get_full_name(),"stop_addr",this.stop_addr));
	endfunction : build_phase

	virtual function void write(ubus_transfer trans);
		if(!disable_scoreboard)
		begin
			check_mutex(trans);
		end
	endfunction : write

	virtual function void check_mutex(ubus_transfer trans);
		if(trans.read_write == WRITE)begin
			if(trans.addr == mutex_addr)begin
				this.mutex_memory = trans.data[0];
			end else begin
				`uvm_error(get_full_name(),"Write la o zona interzisa");
			end
		end else if(trans.read_write == READ)begin
			if( !((trans.addr >= this.start_addr &&
							trans.addr <= this.stop_addr) || (trans.addr == this.mutex_addr)) )begin
				`uvm_error(this.get_full_name(), $sformatf("DON'T have permision to READ at addres 'h%0h ",trans.addr));
			end
			case(this.mutex_memory)
				0: begin
					if( (trans.master != "master[0]")  && (trans.master != ""))begin
						`uvm_error(this.get_full_name(),"Master_0 don't have acces to read");
					end
				end
				1: begin
					if( (trans.master != "master[1]")  && (trans.master != ""))begin
						`uvm_error(this.get_full_name(),"Master_1 don't have acces to read");
					end
				end
			endcase
		end
	endfunction
endclass

class Nmasters_mutex_scoreboard extends ubus_example_scoreboard;
		`uvm_component_utils(Nmasters_mutex_scoreboard)
		`uvm_analysis_imp_decl(_reset)
	
	uvm_analysis_imp_reset#(c_ubus_reset_pkt, Nmasters_mutex_scoreboard) reset_collected_export;

	logic[7:0] memory[int unsigned];
	int NrMasters;
	int mutex_addr;
	int start_addr;
	int stop_addr;

	int first_mutex_read;

	function new (string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		reset_collected_export = new("reset_collected_export", this);
		void'(uvm_config_db#(int)::get(null,get_full_name(),"mutex_addr",this.mutex_addr));
		void'(uvm_config_db#(int)::get(null,get_full_name(),"start_addr",this.start_addr));
		void'(uvm_config_db#(int)::get(null,get_full_name(),"stop_addr",this.stop_addr));
		void'(uvm_config_db#(int)::get(null,get_full_name(),"NrMasters",this.NrMasters));

		first_mutex_read = 1;
	endfunction : build_phase

	virtual function void write(ubus_transfer trans);
		if(!disable_scoreboard)
		begin
			check_mutex(trans);
		end
	endfunction : write

	virtual function void write_reset(c_ubus_reset_pkt ubus_reset_pkt);
		if(ubus_reset_pkt.reset_state == RESET_ASSERTED)begin
			first_mutex_read = 1;
		end
	endfunction

	virtual function void check_mutex(ubus_transfer trans);
		if(trans.read_write == READ)begin
			for(int count_data = 0; count_data < trans.size; count_data++)begin
				if( (trans.addr + count_data) != this.mutex_addr  )begin
					`uvm_error(get_type_name(),$sformatf("DON'T have permision to READ at addres 'h%0h ",trans.addr));
				end else if((trans.addr + count_data) == this.mutex_addr)begin
					if( first_mutex_read )begin
						memory[this.mutex_addr] = trans.data[count_data];
						first_mutex_read = 0;
					end else begin
						if(memory[this.mutex_addr] != trans.data[count_data])begin
						end
					end
				end
			end
		end else if(trans.read_write == WRITE)begin
				for(int count_addr = 0; count_addr < trans.size ; count_addr++)begin
					if( !( ((trans.addr+count_addr) >= this.start_addr &&
							(trans.addr+count_addr) <= this.stop_addr) || ((trans.addr+count_addr) == this.mutex_addr) ) )begin
						`uvm_error(this.get_full_name(), $sformatf("DON'T have permission to WRITE at addres 'h%0h ",(trans.addr+count_addr)));
					end else begin	
						if( (trans.addr + count_addr) == this.mutex_addr)begin
							int NextMaster = (memory[this.mutex_addr] == (this.NrMasters-1)) ? 0 : (memory[this.mutex_addr] + 1) ; 
							if(trans.data[count_addr] == NextMaster )begin
								memory[this.mutex_addr] = trans.data[count_addr];
							end else begin
								`uvm_error(get_type_name(),$sformatf("Next master it's incorect:  Master = %0d, Model = %0d", trans.data[count_addr], NextMaster));
							end
						end else begin
						end
					end
				end
		end
	endfunction

endclass

