`ifndef __UBUS_RESET_MONITOR_SV__
`define __UBUS_RESET_MONITOR_SV__

class c_ubus_reset_monitor extends uvm_monitor;
	`uvm_component_utils(c_ubus_reset_monitor)
	
	uvm_analysis_port#(c_ubus_reset_pkt) reset_port;
	c_ubus_reset_pkt ubus_reset_pkt;

	protected virtual reset_if reset_vif;
	protected e_reset_val reset_val;
	protected uvm_event reset_ev;
	
	function new(string name = "ubus_reset_monitor", uvm_component parent = null);
		super.new(name,parent);
	endfunction : new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		if(!uvm_config_db#(virtual reset_if)::get(null,"","reset_vif", reset_vif))begin
			`uvm_fatal(get_type_name(),"Invalid Reset_vif");
		end
		if(!uvm_config_db#(e_reset_val)::get(null, get_full_name(),"reset_val", reset_val))begin
			`uvm_fatal(get_type_name(), "Undefined Reset_Val");
		end
		
		reset_port = new("reset_port", this);
		reset_ev = uvm_event_pool::get_global("reset_ev");
	endfunction : build_phase

	virtual task run_phase(uvm_phase phase);
		forever begin
			ubus_reset_pkt = c_ubus_reset_pkt::type_id::create("ubus_reset_pkt");
			forever begin
				@(posedge reset_vif.sig_clock);
				if(reset_val == ACTIVE_LOW && reset_vif.reset === 1'b0)begin
					break;
				end else if(reset_val == ACTIVE_HIGH && reset_vif.reset === 1'b1)begin
					break;
				end
			end
			`uvm_info(get_type_name(),"Reset Asserted", UVM_LOW);
			ubus_reset_pkt.reset_state = RESET_ASSERTED;
			reset_ev.trigger();
			reset_port.write(ubus_reset_pkt);
			
			forever begin
				@(posedge reset_vif.sig_clock);
				if(reset_val == ACTIVE_LOW && reset_vif.reset === 1'b1)begin
					break;
				end else if(reset_val == ACTIVE_HIGH && reset_vif.reset === 1'b0)begin
					break;
				end
			end
			`uvm_info(get_type_name(),"Reset Deasserted", UVM_LOW);
		end
	endtask

endclass

`endif
