`ifndef __VIRTUAL_SEQUENCER_SV__
`define __VIRTUAL_SEQUENCER_SV__

typedef uvm_sequencer#(ubus_transfer) master_sequencer_type;
typedef uvm_sequencer#(ubus_transfer) slave_sequencer_type;

class c_virtual_sequencer extends uvm_sequencer;
    `uvm_component_utils (c_virtual_sequencer)

    master_sequencer_type masters_sequencer[$];
    slave_sequencer_type slaves_sequencer[$];

    function new(string name = "virtual sequencer", uvm_component parent = null);
        super.new(name,parent);
    endfunction

endclass

`endif