


// Base Test
class ubus_example_base_test extends uvm_test;

  `uvm_component_utils(ubus_example_base_test)

  ubus_example_tb ubus_example_tb0;
  uvm_table_printer printer;
  bit test_pass = 1;

  c_virtual_seq virtual_seq;

  function new(string name = "ubus_example_base_test", 
    uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    // Enable transaction recording for everything
    uvm_config_db#(int)::set(this, "*", "recording_detail", UVM_FULL);
    // Create the tb
    ubus_example_tb0 = ubus_example_tb::type_id::create("ubus_example_tb0", this);
    // Create a specific depth printer for printing the created topology
    printer = new();
    printer.knobs.depth = 3;
	  
	//vom seta variabilele pentru secventa de reset
	uvm_config_db#(uvm_object_wrapper)::set(this,"ubus_example_tb0.ubus0.ubus_reset_agent.ubus_reset_sequencer.main_phase", 
							 "default_sequence",c_ubus_reset_seq::type_id::get());
	  
	//voi seta varibilele pentru componentele agentului de reset
	uvm_config_db#(e_reset_val)::set(this,"ubus_example_tb0.ubus0","reset_val",ACTIVE_HIGH);
	
	//vom crea secventa virtuala
	virtual_seq = c_virtual_seq::type_id::create("virtual_sequence");

  endfunction : build_phase

  function void end_of_elaboration_phase(uvm_phase phase);
    // Set verbosity for the bus monitor for this demo
     if(ubus_example_tb0.ubus0.bus_monitor != null)
       ubus_example_tb0.ubus0.bus_monitor.set_report_verbosity_level(UVM_FULL);
    `uvm_info(get_type_name(),
      $sformatf("Printing the test topology :\n%s", this.sprint(printer)), UVM_LOW)
  endfunction : end_of_elaboration_phase

  task run_phase(uvm_phase phase);
    //set a drain-time for the environment if desired
	phase.phase_done.set_drain_time(this, 50);
	phase.raise_objection(this);
	virtual_seq.start(ubus_example_tb0.ubus0.virt_sequencer);
	phase.drop_objection(this);
  endtask : run_phase

  function void extract_phase(uvm_phase phase);
    if(ubus_example_tb0.scoreboard0.sbd_error)
      test_pass = 1'b0;
  endfunction // void
  
  function void report_phase(uvm_phase phase);
    if(test_pass) begin
      `uvm_info(get_type_name(), "** UVM TEST PASSED **", UVM_NONE)
    end
    else begin
      `uvm_error(get_type_name(), "** UVM TEST FAIL **")
    end
  endfunction

endclass : ubus_example_base_test


// Read Modify Write Read Test
class test_read_modify_write extends ubus_example_base_test;

  `uvm_component_utils(test_read_modify_write)

  function new(string name = "test_read_modify_write", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
  begin
    uvm_config_db#(uvm_object_wrapper)::set(this,
		    "ubus_example_tb0.ubus0.masters[0].sequencer.run_phase", 
			       "default_sequence",
				read_modify_write_seq::type_id::get());
    uvm_config_db#(uvm_object_wrapper)::set(this,
		    "ubus_example_tb0.ubus0.slaves[0].sequencer.run_phase", 
			       "default_sequence",
				slave_memory_seq::type_id::get());
    // Create the tb
    super.build_phase(phase);
  end
  endfunction : build_phase

endclass : test_read_modify_write


// Large word read/write test
class test_r8_w8_r4_w4 extends ubus_example_base_test;

  `uvm_component_utils(test_r8_w8_r4_w4)

  function new(string name = "test_r8_w8_r4_w4", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
  begin
    super.build_phase(phase);
    uvm_config_db#(uvm_object_wrapper)::set(this,
		      "ubus_example_tb0.ubus0.masters[0].sequencer.run_phase", 
			       "default_sequence",
				r8_w8_r4_w4_seq::type_id::get());
    uvm_config_db#(uvm_object_wrapper)::set(this,
		      "ubus_example_tb0.ubus0.slaves[0].sequencer.run_phase", 
			       "default_sequence",
				slave_memory_seq::type_id::get());
  end
  endfunction : build_phase

endclass : test_r8_w8_r4_w4 


// 2 Master, 4 Slave test
class test_2m_4s extends ubus_example_base_test;

  `uvm_component_utils(test_2m_4s)

  function new(string name = "test_2m_4s", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
     loop_read_modify_write_seq lrmw_seq;

  begin
    // Overides to the ubus_example_tb build_phase()
    // Set the topology to 2 masters, 4 slaves
    uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0", 
			       "num_masters", 2);
    uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0", 
			       "num_slaves", 4);
     
   // Control the number of RMW loops
    uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.masters[0].sequencer.loop_read_modify_write_seq", "itr", 6);
    uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.masters[1].sequencer.loop_read_modify_write_seq", "itr", 8);

     // Define the sequences to run in the run phase
    uvm_config_db#(uvm_object_wrapper)::set(this,"*.ubus0.masters[0].sequencer.main_phase", 
			       "default_sequence",
				loop_read_modify_write_seq::type_id::get());
     lrmw_seq = loop_read_modify_write_seq::type_id::create();
    uvm_config_db#(uvm_sequence_base)::set(this,
			       "ubus_example_tb0.ubus0.masters[1].sequencer.main_phase", 
			       "default_sequence",
				lrmw_seq);

     for(int i = 0; i < 4; i++) begin
	string slname;
	$swrite(slname,"ubus_example_tb0.ubus0.slaves[%0d].sequencer", i);
	uvm_config_db#(uvm_object_wrapper)::set(this, {slname,".run_phase"}, 
						  "default_sequence",
						  slave_memory_seq::type_id::get());
     end
     
    // Create the tb
    super.build_phase(phase);
  end
  endfunction : build_phase

  function void connect_phase(uvm_phase phase);
    // Connect other slaves monitor to scoreboard
    ubus_example_tb0.ubus0.slaves[1].monitor.item_collected_port.connect(
      ubus_example_tb0.scoreboard0.item_collected_export);
    ubus_example_tb0.ubus0.slaves[2].monitor.item_collected_port.connect(
      ubus_example_tb0.scoreboard0.item_collected_export);
    ubus_example_tb0.ubus0.slaves[3].monitor.item_collected_port.connect(
      ubus_example_tb0.scoreboard0.item_collected_export);
  endfunction : connect_phase
  
  function void end_of_elaboration_phase(uvm_phase phase);
    // Set up slave address map for ubus0 (slaves[0] is overwritten here)
    ubus_example_tb0.ubus0.set_slave_address_map("slaves[0]", 16'h0000, 16'h3fff);
    ubus_example_tb0.ubus0.set_slave_address_map("slaves[1]", 16'h4000, 16'h7fff);
    ubus_example_tb0.ubus0.set_slave_address_map("slaves[2]", 16'h8000, 16'hBfff);
    ubus_example_tb0.ubus0.set_slave_address_map("slaves[3]", 16'hC000, 16'hFfff);
     super.end_of_elaboration_phase(phase);
  endfunction : end_of_elaboration_phase

endclass : test_2m_4s

class test_2m_1s extends ubus_example_base_test;
	`uvm_component_utils(test_2m_1s)

	slave_mutex_seq slave_mutex_seq_inst;

	function new(string name = "test_2m_1s", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0", 
			       				"num_masters", 2);
    	uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0",
	    						"num_slaves", 1);
		
		for(int count_masters = 0; count_masters < 2; count_masters++ )begin
			string master_seq_path = $sformatf("ubus_example_tb0.ubus0.masters[%0d].sequencer.loop_read_protected_zone_seq",count_masters);
			uvm_config_db#(int)::set(this,master_seq_path,"master_id",count_masters);
			uvm_config_db#(int)::set(this,master_seq_path,"nr_itr",2);
			uvm_config_db#(int)::set(this,master_seq_path,"mutex_addr",0);
			uvm_config_db#(int)::set(this,master_seq_path,"start_addr",16'h000A);
			uvm_config_db#(int)::set(this,master_seq_path,"stop_addr",16'h000C);
		end
		
		//setez variabilele interne pentru slave sequence;
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","mutex_addr",0);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","start_addr",0);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","stop_addr",12);
		
		//setez valorile in scoreboard
		uvm_config_db#(int)::set(this,"ubus_example_tb0.scoreboard0","mutex_addr",0);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.scoreboard0","start_addr",10);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.scoreboard0","stop_addr",12);
		
		//startez secventele pentru master;
		for(int count_masters = 0; count_masters < 2; count_masters++ )begin
			string seq_phase = $sformatf("ubus_example_tb0.ubus0.masters[%0d].sequencer.main_phase",count_masters);
			uvm_config_db#(uvm_object_wrapper)::set(this,seq_phase, "default_sequence",
													loop_read_protected_zone_seq::type_id::get());
		end
		
		set_type_override_by_type(ubus_example_scoreboard::get_type(), mutex_scoreboard::get_type());
		slave_mutex_seq_inst = slave_mutex_seq::type_id::create("slave_mutex_seq_inst");
		
	endfunction : build_phase

	function void end_of_elaboration_phase(uvm_phase phase);
		 ubus_example_tb0.ubus0.set_slave_address_map("slaves[0]", 32'h0000, 32'h000C);
	endfunction
	
	virtual task run_phase(uvm_phase phase);
		fork
			slave_mutex_seq_inst.start(ubus_example_tb0.ubus0.slaves[0].sequencer);
		join
	endtask
endclass : test_2m_1s

class test_nm_1s extends ubus_example_base_test;
	`uvm_component_utils(test_nm_1s)

	slave_mutex_seq slave_mutex_seq_inst;
	loop_write_protected_zone_seq loop_write_seq[];
	int NrMasters;

	function new(string name = "test_nm_1s", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!$value$plusargs("TestN_Masters=%0d", NrMasters))begin
			`uvm_error(get_type_name(), "Undefined TestN_Masters from command line");
		end else if(NrMasters > `N_MASTERS)begin
			`uvm_fatal(get_type_name(),"TesN_Masters(simulation command line var) != N_Masters(compilation command line var)");
		end
		
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0", 
			       				"num_masters", NrMasters);
    	uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0",
	    						"num_slaves", 1);
		
		for(int count_masters = 0; count_masters < NrMasters; count_masters++ )begin
			string master_seq_path = $sformatf("ubus_example_tb0.ubus0.masters[%0d].sequencer.loop_write_seq_%0d",count_masters,count_masters);
			uvm_config_db#(int)::set(this,master_seq_path,"master_id",count_masters);
			uvm_config_db#(int)::set(this,master_seq_path,"NrMasters",NrMasters);
			uvm_config_db#(int)::set(this,master_seq_path,"nr_itr",2);
			uvm_config_db#(int)::set(this,master_seq_path,"mutex_addr",0);
			uvm_config_db#(int)::set(this,master_seq_path,"start_addr",16'h000A);
			uvm_config_db#(int)::set(this,master_seq_path,"stop_addr",16'h000C);
		end
		
		//setez variabilele interne pentru slave sequence;
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","mutex_addr",0);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","start_addr",0);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","stop_addr",12);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","start_id",0);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.slaves[0].sequencer.slave_mutex_seq_inst","stop_id", NrMasters-1);
		
		//setez valorile in scoreboard
		uvm_config_db#(int)::set(this,"ubus_example_tb0.scoreboard0","mutex_addr",0);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.scoreboard0","start_addr",10);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.scoreboard0","stop_addr",12);
		uvm_config_db#(int)::set(this,"ubus_example_tb0.scoreboard0","NrMasters",NrMasters);
		
		//startez secventele pentru master;
		if(this.NrMasters > 0)begin
			loop_write_seq = new[NrMasters];
			for(int count_masters = 0; count_masters < NrMasters; count_masters++)begin
				loop_write_seq[count_masters] = loop_write_protected_zone_seq::type_id::create( $sformatf("loop_write_seq_%0d",count_masters));
			end
		end

		set_type_override_by_type(ubus_example_scoreboard::get_type(), Nmasters_mutex_scoreboard::get_type());
		slave_mutex_seq_inst = slave_mutex_seq::type_id::create("slave_mutex_seq_inst");
		
		//vom seta numarul de reset-uri pe intervalul simularii, diferit de reset-ul inital
		uvm_config_db#(int)::set(this,"ubus_example_tb0.ubus0.ubus_reset_agent.ubus_reset_sequencer.c_ubus_reset_seq","nr_reset_during_sim",5);
	endfunction : build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		virtual_seq.slave_sequences.push_front(slave_mutex_seq_inst);

		for(int i = 0; i < NrMasters; i++)begin
			virtual_seq.master_sequences.push_front(loop_write_seq[i]);
		end
	endfunction : connect_phase

	function void end_of_elaboration_phase(uvm_phase phase);
		 ubus_example_tb0.ubus0.set_slave_address_map("slaves[0]", 32'h0000, 32'h000C);
	endfunction

endclass


