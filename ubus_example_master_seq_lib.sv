
class incr_read_byte_seq extends ubus_base_sequence;

	function new(string name="incr_read_byte_seq");
		super.new(name);
	endfunction : new

	`uvm_object_utils(incr_read_byte_seq)

	read_byte_seq read_byte_seq0;

	rand int unsigned count;
	constraint count_ct { (count < 20); }
	rand bit [15:0] start_address;
	rand int unsigned incr_transmit_del = 0;
	constraint transmit_del_ct { (incr_transmit_del <= 10); }

	virtual task body();
		`uvm_info(get_type_name(),
			$sformatf("%s starting with count = %0d",
				get_sequence_path(), count), UVM_MEDIUM);
		repeat(count) begin : repeat_block
			`uvm_do_with(read_byte_seq0,
				{ read_byte_seq0.start_addr == start_address;
					read_byte_seq0.transmit_del == incr_transmit_del; } )
			start_address++;
		end : repeat_block
	endtask : body

endclass : incr_read_byte_seq


//------------------------------------------------------------------------------
//
// SEQUENCE: incr_write_byte_seq
//
//------------------------------------------------------------------------------

class incr_write_byte_seq extends ubus_base_sequence;

	function new(string name="incr_write_byte_seq");
		super.new(name);
	endfunction : new

	`uvm_object_utils(incr_write_byte_seq)

	write_byte_seq write_byte_seq0;

	rand int unsigned count;
	constraint count_ct { (count < 20); }
	rand bit [15:0] start_address;
	rand int unsigned incr_transmit_del = 0;
	constraint transmit_del_ct { (incr_transmit_del <= 10); }

	virtual task body();
		`uvm_info(get_type_name(),
			$sformatf("%s starting with count = %0d",
				get_sequence_path(), count), UVM_MEDIUM);
		repeat(count) begin : repeat_block
			`uvm_do_with(write_byte_seq0,
				{ write_byte_seq0.start_addr == start_address;
					write_byte_seq0.transmit_del == incr_transmit_del; } )
			start_address++;
		end : repeat_block
	endtask : body

endclass : incr_write_byte_seq


//------------------------------------------------------------------------------
//
// SEQUENCE: incr_read_write_read_seq
//
//------------------------------------------------------------------------------

class incr_read_write_read_seq extends ubus_base_sequence;

	function new(string name="incr_read_write_read_seq");
		super.new(name);
	endfunction : new

	`uvm_object_utils(incr_read_write_read_seq)

	incr_read_byte_seq  read0;
	incr_write_byte_seq write0;

	virtual task body();
		`uvm_info(get_type_name(),
			$sformatf("%s starting sequence",
				get_sequence_path()), UVM_MEDIUM);
		`uvm_do(read0)
		`uvm_do(write0)
		`uvm_do(read0)
	endtask : body

endclass : incr_read_write_read_seq


//------------------------------------------------------------------------------
//
// SEQUENCE: r8_w8_r4_w4_seq
//
//------------------------------------------------------------------------------

class r8_w8_r4_w4_seq extends ubus_base_sequence;

	function new(string name="r8_w8_r4_w4_seq");
		super.new(name);
	endfunction : new

	`uvm_object_utils(r8_w8_r4_w4_seq)

	read_word_seq read_word_seq0;
	read_double_word_seq read_double_word_seq0;
	write_word_seq write_word_seq0;
	write_double_word_seq write_double_word_seq0;

	rand bit [15:0] start_address;

	constraint start_address_ct { (start_address == 16'h4000); }

	virtual task body();
		`uvm_info(get_type_name(),
			$sformatf("%s starting...",
				get_sequence_path()), UVM_MEDIUM);
		`uvm_do_with(read_double_word_seq0,
			{ read_double_word_seq0.start_addr == start_address;
				read_double_word_seq0.transmit_del == 2; } )
		`uvm_do_with(write_double_word_seq0,
			{ write_double_word_seq0.start_addr == start_address;
				write_double_word_seq0.transmit_del == 4; } )
		start_address = start_address + 8;
		`uvm_do_with(read_word_seq0,
			{ read_word_seq0.start_addr == start_address;
				read_word_seq0.transmit_del == 6; } )
		`uvm_do_with(write_word_seq0,
			{ write_word_seq0.start_addr == start_address;
				write_word_seq0.transmit_del == 8; } )
	endtask : body

endclass : r8_w8_r4_w4_seq


//------------------------------------------------------------------------------
//
// SEQUENCE: read_modify_write_seq
//
//------------------------------------------------------------------------------

class read_modify_write_seq extends ubus_base_sequence;

	function new(string name="read_modify_write_seq");
		super.new(name);
	endfunction : new

	`uvm_object_utils(read_modify_write_seq)

	read_byte_seq read_byte_seq0;
	write_byte_seq write_byte_seq0;

	rand bit [15:0] addr_check;
	bit [7:0] m_data0_check;

	virtual task body();
		`uvm_info(get_type_name(),
			$sformatf("%s starting...",
				get_sequence_path()), UVM_MEDIUM);
		// READ A RANDOM LOCATION
		`uvm_do_with(read_byte_seq0, {read_byte_seq0.transmit_del == 0; })
		addr_check = read_byte_seq0.rsp.addr;
		m_data0_check = read_byte_seq0.rsp.data[0] + 1;
		// WRITE MODIFIED READ DATA
		`uvm_do_with(write_byte_seq0,
			{ write_byte_seq0.start_addr == addr_check;
				write_byte_seq0.data0 == m_data0_check; } )
		// READ MODIFIED WRITE DATA
		`uvm_do_with(read_byte_seq0,
			{ read_byte_seq0.start_addr == addr_check; } )
		assert(m_data0_check == read_byte_seq0.rsp.data[0]) else
			`uvm_error(get_type_name(),
				$sformatf("%s Read Modify Write Read error!\n\tADDR: %h, EXP: %h, ACT: %h",
					get_sequence_path(),addr_check,m_data0_check,read_byte_seq0.rsp.data[0]));
	endtask : body

endclass : read_modify_write_seq


//------------------------------------------------------------------------------
//
// SEQUENCE: loop_read_modify_write_seq
//
//------------------------------------------------------------------------------

class loop_read_modify_write_seq extends ubus_base_sequence;

	int itr;

	function new(string name="loop_read_modify_write_seq");
		super.new(name);
	endfunction : new

	`uvm_object_utils(loop_read_modify_write_seq)

	read_modify_write_seq rmw_seq;

	virtual task body();
		void'(uvm_config_db#(int)::get(null,get_full_name(),"itr", itr));
		`uvm_info(get_type_name(),
			$sformatf("%s starting...itr = %0d",
				get_sequence_path(),itr), UVM_NONE);
		for(int i = 0; i < itr; i++) begin
			`uvm_do(rmw_seq)
		end
	endtask : body

endclass : loop_read_modify_write_seq

class loop_read_protected_zone_seq extends ubus_base_sequence;
	`uvm_object_utils(loop_read_protected_zone_seq)

	int master_id;
	int nr_itr;
	int mutex_addr;
	int start_addr;
	int stop_addr;

	function new(string name = "loop_write_protectec_zone_seq");
		super.new(name);
	endfunction

	virtual task body();
		void'(uvm_config_db#(int)::get(null, get_full_name(),"master_id",master_id));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"nr_itr",nr_itr));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"mutex_addr",mutex_addr));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"start_addr",start_addr));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"stop_addr",stop_addr));

		for(int count_itr = 0; count_itr < nr_itr; count_itr++)begin
			do begin
				//voi executa o citire de la adresa mutex-ului;
				`uvm_do_with(req,{ req.read_write == READ;
						req.addr == this.mutex_addr;
						req.size == 1;
						req.transmit_delay inside {[1:5]};})
				get_response(rsp);
			end while(rsp.data[0] != this.master_id);
			//voi realiza o scriere la zona protejata

			for(int count_read = start_addr; count_read <= stop_addr; count_read++ )begin
				`uvm_do_with(req,{req.read_write == READ;
						req.addr == count_read;
						req.size == 1;
						req.transmit_delay inside {[1:5]} ;})
			end

			//facem o scriere pentru a da permisiunea celuilalt master
			`uvm_do_with(req,{req.read_write == WRITE;
					req.addr == this.mutex_addr;
					req.data[0] == (this.master_id == 0? 1:0);
					req.size == 1;
					req.error_pos == 1000;
					req.transmit_delay inside {[1:5]};})

		end
	endtask
endclass

class loop_write_protected_zone_seq extends ubus_base_sequence;
	`uvm_object_utils(loop_write_protected_zone_seq)

	int master_id;
	int NrMasters;
	int nr_itr;
	int mutex_addr;
	int start_addr;
	int stop_addr;

	function new(string name = "loop_write_protected_zone_seq");
		super.new(name);
	endfunction

	virtual task body();
		void'(uvm_config_db#(int)::get(null, get_full_name(),"master_id",master_id));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"NrMasters",NrMasters));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"nr_itr",nr_itr));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"mutex_addr",mutex_addr));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"start_addr",start_addr));
		void'(uvm_config_db#(int)::get(null, get_full_name(),"stop_addr",stop_addr));
		
		for(int count_itr = 0; count_itr < nr_itr; count_itr++)begin
			int CountNrRead = 0;
			do begin
				//voi executa o citire de la adresa mutex-ului pana ce am primit id-ul corespunzator;
				`uvm_do_with(req,{ req.read_write == READ;
						req.addr == this.mutex_addr;
						req.size == 1;
						req.transmit_delay == 0;})
				get_response(rsp);
				CountNrRead++;
			end while(rsp.data[0] != this.master_id);

			//voi realiza scrieri la adresa mutexului(<itr_curent>, <nr citiri zona protejata>, <in total>)
			for(int count_write_addr = start_addr; count_write_addr <= stop_addr; count_write_addr++ )begin
				`uvm_do_with(req,{req.read_write == WRITE;
						req.addr == count_write_addr;
						req.data[0] == ((count_write_addr == start_addr)? count_itr :
							(count_write_addr == (start_addr + 1))? CountNrRead :
							(count_write_addr == (start_addr + 2))? (count_itr+CountNrRead) : 8'hFF);
						req.size == 1;
						req.error_pos == 1000;
						req.transmit_delay == 0;})
				get_response(rsp);
			end

			//facem o scriere pentru a da permisiunea urmatorului master
			`uvm_do_with(req,{req.read_write == WRITE;
					req.addr == this.mutex_addr;
					req.data[0] == (this.master_id == (this.NrMasters-1)? 0 : (this.master_id+1));
					req.size == 1;
					req.error_pos == 1000;
					req.transmit_delay == 0;})
			get_response(rsp);
		end
	endtask

endclass

