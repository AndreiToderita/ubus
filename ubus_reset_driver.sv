`ifndef __RESET_DRIVER_SV__
`define __RESET_DRIVER_SV__

class c_ubus_reset_driver extends uvm_driver#(c_ubus_reset_pkt);
	`uvm_component_utils(c_ubus_reset_driver)

	protected virtual reset_if reset_vif;
	protected e_reset_val reset_val;

	function new(string name = "ubus_reset_driver", uvm_component parent = null);
		super.new(name,parent);
	endfunction : new

	function void build_phase(uvm_phase phase);
		super.connect_phase(phase);
		if(!uvm_config_db#(virtual reset_if)::get(null,"","reset_vif", reset_vif))begin
			`uvm_fatal(get_type_name(),"Invalid Reset_vif");
		end
		if(!uvm_config_db#(e_reset_val)::get(null,get_full_name(),"reset_val", reset_val))begin
			`uvm_fatal(get_type_name(), "Undefined Reset_Val");
		end
	endfunction : build_phase
	
	virtual task run_phase(uvm_phase phase);
		//Vom initializa semnalele
		init_signal();
		forever begin
			seq_item_port.get_next_item(req);
			
			//asteptam un timp pana la reset
			repeat(req.transmit_time)
				@(posedge reset_vif.sig_clock);

			//activam reset-ul
			repeat(req.reset_time)begin
				get_reset();
				@(posedge reset_vif.sig_clock);
			end

			//vom dezactiva reset-ul
			init_signal();

			seq_item_port.item_done();
		end
	endtask : run_phase

	function void init_signal();
		if(this.reset_val == ACTIVE_HIGH)begin
			reset_vif.reset <= 1'b0;
		end else begin
			reset_vif.reset <= 1'b1;
		end
	endfunction
	
	function void get_reset();
		if(this.reset_val == ACTIVE_HIGH)begin
			reset_vif.reset <= 1'b1;
		end else begin
			reset_vif.reset <= 1'b0;
		end
	endfunction
endclass : c_ubus_reset_driver
`endif