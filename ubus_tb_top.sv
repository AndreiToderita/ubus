`define UBUS_ADDR_WIDTH 16

`include "ubus_pkg.sv"
`include "dut_dummy.v"
`include "ubus_if.sv"
`include "reset_if.sv"


module ubus_tb_top;
  import uvm_pkg::*;
  import ubus_pkg::*;
  `include "test_lib.sv"
  
  wire tmp_reset;
  wire tmp_clock;
  
  ubus_if#() vif(tmp_reset,tmp_clock); // SystemVerilog Interface
  reset_if reset_vif(tmp_clock,tmp_reset);
  
  dut_dummy dut(
    vif.sig_request,
    vif.sig_grant,
    vif.sig_clock,
    vif.sig_reset,
    vif.sig_addr,
    vif.sig_size,
    vif.sig_read,
    vif.sig_write,
    vif.sig_start,
    vif.sig_bip,
    vif.sig_data,
    vif.sig_wait,
    vif.sig_error
  );

  initial begin automatic uvm_coreservice_t cs_ = uvm_coreservice_t::get();
    uvm_config_db#(virtual ubus_if)::set(cs_.get_root(), "*", "vif", vif);
	uvm_config_db#(virtual reset_if)::set(cs_.get_root(), "*", "reset_vif", reset_vif);
    run_test();
  end

  initial begin    
    $dumpfile("dump.vcd");
    $dumpvars;
    //vif.sig_reset <= 1'b1;
    vif.sig_clock <= 1'b1;
    //#51 vif.sig_reset = 1'b0;
  end

  //Generate Clock
  always
    #5 vif.sig_clock = ~vif.sig_clock;

endmodule
